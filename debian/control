Source: libatomic-queue
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>,
           Andreas Tille <tille@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: libs
Priority: optional
Build-Depends: debhelper-compat (= 13),
               d-shlibs,
               libboost-dev,
               libboost-test-dev,
               libconcurrentqueue-dev,
               libreaderwriterqueue-dev,
               libtbb-dev,
               libxenium-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/libatomic-queue
Vcs-Git: https://salsa.debian.org/med-team/libatomic-queue.git
Homepage: https://github.com/max0x7ba/atomic_queue
Rules-Requires-Root: no

Package: libatomic-queue0
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: C++ atomic_queue library
 C++11 multiple-producer-multiple-consumer lockless queues based on
 circular buffer with std::atomic.  The main design principle these
 queues follow is simplicity: the bare minimum of atomic operations,
 fixed size buffer, value semantics.
 .
 The circular buffer side-steps the memory reclamation problem inherent
 in linked-list based queues for the price of fixed buffer size. See
 Effective memory reclamation for lock-free data structures in C++
 for more details.
 .
 These qualities are also limitations:
 .
  * The maximum queue size must be set at compile time or construction time.
  * There are no OS-blocking push/pop functions.
 .
 Nevertheless, ultra-low-latency applications need just that and nothing
 more. The simplicity pays off, see the throughput and latency benchmarks.
 .
 Available containers are:
 .
  * AtomicQueue - a fixed size ring-buffer for atomic elements.
  * OptimistAtomicQueue - a faster fixed size ring-buffer for atomic
    elements which busy-waits when empty or full.
  * AtomicQueue2 - a fixed size ring-buffer for non-atomic elements.
  * OptimistAtomicQueue2 - a faster fixed size ring-buffer for non-atomic
    elements which busy-waits when empty or full.
 .
 These containers have corresponding AtomicQueueB, OptimistAtomicQueueB,
 AtomicQueueB2, OptimistAtomicQueueB2 versions where the buffer size is
 specified as an argument to the constructor.
 .
 This package contains the dynamic library.

Package: libatomic-queue-dev
Architecture: any
Section: libdevel
Depends: libatomic-queue0 (= ${binary:Version}),
         libboost-dev,
         ${shlibs:Depends},
         ${misc:Depends}
Description: devel files for C++ atomic_queue library
 C++11 multiple-producer-multiple-consumer lockless queues based on
 circular buffer with std::atomic.  The main design principle these
 queues follow is simplicity: the bare minimum of atomic operations,
 fixed size buffer, value semantics.
 .
 The circular buffer side-steps the memory reclamation problem inherent
 in linked-list based queues for the price of fixed buffer size. See
 Effective memory reclamation for lock-free data structures in C++
 for more details.
 .
 These qualities are also limitations:
 .
  * The maximum queue size must be set at compile time or construction time.
  * There are no OS-blocking push/pop functions.
 .
 Nevertheless, ultra-low-latency applications need just that and nothing
 more. The simplicity pays off, see the throughput and latency benchmarks.
 .
 Available containers are:
 .
  * AtomicQueue - a fixed size ring-buffer for atomic elements.
  * OptimistAtomicQueue - a faster fixed size ring-buffer for atomic
    elements which busy-waits when empty or full.
  * AtomicQueue2 - a fixed size ring-buffer for non-atomic elements.
  * OptimistAtomicQueue2 - a faster fixed size ring-buffer for non-atomic
    elements which busy-waits when empty or full.
 .
 These containers have corresponding AtomicQueueB, OptimistAtomicQueueB,
 AtomicQueueB2, OptimistAtomicQueueB2 versions where the buffer size is
 specified as an argument to the constructor.
 .
 This package contains the header files and static library.
