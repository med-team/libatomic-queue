Author: Nilesh Patra <npatra974@gmail.com>,
        Andreas Tille <tille@debian.org>
Reviewed-By: Étienne Mollier <emollier@debian.org>
Last-Update: 2024-08-23
Description: add rules to generate a shared library.

--- libatomic-queue.orig/Makefile
+++ libatomic-queue/Makefile
@@ -19,6 +19,7 @@
 TOOLSET := gcc
 
 build_dir := ${CURDIR}/build/${BUILD}/${TOOLSET}
+build_dir_shared := ${CURDIR}/build_shared/${BUILD}/${TOOLSET}
 
 cxx.gcc := g++
 cc.gcc := gcc
@@ -75,7 +76,8 @@
 PREPROCESS.CXX = ${CXX} -o $@ -E ${cppflags} ${cxxflags} $(abspath $<)
 COMPILE.C = ${CC} -o $@ -c ${cppflags} ${cflags} -MD -MP $(abspath $<)
 LINK.EXE = ${LD} -o $@ $(ldflags) $(filter-out ${relink},$^) $(ldlibs)
-LINK.SO = ${LD} -o $@ -shared $(ldflags) $(filter-out ${relink},$^) $(ldlibs)
+SOVERSION := 0
+LINK.SO = ${LD} -o $@.$(SOVERSION) -shared -Wl,-soname,`basename $@`.$(SOVERSION) $(ldflags) $(filter-out Makefile,$^) $(ldlibs)
 LINK.A = ${AR} rscT $@ $(filter-out ${relink},$^)
 
 ifneq (,$(findstring n,$(firstword -${MAKEFLAGS})))
@@ -90,9 +92,17 @@
 
 all : ${exes}
 
-${exes} : % : ${build_dir}/%
+${exes} : % : ${build_dir}/% ${build_dir}/libatomic_queue.a  ${build_dir_shared}/libatomic_queue.so
 	ln -sf ${<:${CURDIR}/%=%}
 
+${build_dir}/libatomic_queue.a : $(addprefix ${build_dir}/,cpu_base_frequency.o huge_pages.o)
+-include ${build_dir}/cpu_base_frequency.d
+-include ${build_dir}/huge_pages.d
+
+${build_dir_shared}/libatomic_queue.so : $(addprefix ${build_dir_shared}/,cpu_base_frequency.o huge_pages.o)
+-include ${build_dir_shared}/cpu_base_frequency.d
+-include ${build_dir_shared}/huge_pages.d
+
 benchmarks_src := benchmarks.cc cpu_base_frequency.cc huge_pages.cc
 ${build_dir}/benchmarks : cppflags += ${cppflags.tbb} ${cppflags.moodycamel} ${cppflags.xenium}
 ${build_dir}/benchmarks : ldlibs += ${ldlibs.tbb} ${ldlibs.moodycamel} ${ldlibs.xenium} -ldl
@@ -112,9 +122,10 @@
 	$(call strip2,${LINK.EXE})
 -include ${example_src:%.cc=${build_dir}/%.d}
 
-${build_dir}/%.so : cxxflags += -fPIC
-${build_dir}/%.so : ${relink} | ${build_dir}
-	$(call strip2,${LINK.SO})
+${build_dir_shared}/%.so : cxxflags += -fPIC
+${build_dir_shared}/%.so : Makefile | ${build_dir_shared}
+	${LINK.SO}
+	ln -s `basename $@`.$(SOVERSION) $@
 
 ${build_dir}/%.a : ${relink} | ${build_dir}
 	$(call strip2,${LINK.A})
@@ -125,6 +136,12 @@
 ${build_dir}/%.o : src/%.c ${recompile} | ${build_dir}
 	$(call strip2,${COMPILE.C})
 
+${build_dir_shared}/%.o : src/%.cc Makefile | ${build_dir_shared}
+	$(call strip2,${COMPILE.CXX})
+
+${build_dir_shared}/%.o : src/%.c Makefile | ${build_dir_shared}
+	$(call strip2,${COMPILE.C})
+
 ${build_dir}/%.S : cppflags += ${cppflags.tbb} ${cppflags.moodycamel} ${cppflags.xenium}
 ${build_dir}/%.S : src/%.cc ${recompile} | ${build_dir}
 	$(call strip2,${COMPILE.S})
@@ -139,6 +156,10 @@
 ${build_dir} ${build_dir}/.make:
 	mkdir -p $@
 
+${build_dir_shared}/.make : | ${build_dir_shared}
+${build_dir_shared} ${build_dir_shared}/.make:
+	mkdir -p $@
+
 ver = "$(shell ${1} --version | head -n1)"
 # Trigger recompilation when compiler environment change.
 env.compile := $(call ver,${CXX}) ${cppflags} ${cxxflags} ${cppflags.tbb} ${cppflags.moodycamel} ${cppflags.xenium}
@@ -175,6 +196,7 @@
 
 clean :
 	rm -rf ${build_dir} ${exes}
+	rm -rf ${build_dir_shared}
 
 versions:
 	${MAKE} --version | awk 'FNR<2'
